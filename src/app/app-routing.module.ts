import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';

import { SitesComponent } from './components/sites/sites.component';

import { SiteComponent } from './components/site/site.component';
import { UtilizadoresComponent } from './components/utilizadores/utilizadores.component';
import { PaginasComponent } from './components/paginas/paginas.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { ArtigosComponent } from './components/artigos/artigos.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { MenusComponent } from './components/menus/menus.component';
import { ConteudosComponent } from './components/conteudos/conteudos.component';

import { CreateSiteComponent } from './components/createsite/createsite.component';

import { EditSiteComponent } from './components/editsite/editsite.component';

import { CreatePaginaComponent } from './components/createpagina/createpagina.component';
import { EditPaginaComponent } from './components/editpagina/editpagina.component';

import { CreateCategoriaComponent } from './components/createcategoria/createcategoria.component';
import { EditCategoriaComponent } from './components/editcategoria/editcategoria.component';

import { CreateArtigoComponent } from './components/createartigo/createartigo.component';
import { EditArtigoComponent } from './components/editartigo/editartigo.component';

import { CreateEventoComponent } from './components/createevento/createevento.component';

import { EditMenuComponent } from './components/editmenu/editmenu.component';

import { CreateConteudoComponent } from './components/createconteudo/createconteudo.component';
import { EditConteudoComponent } from './components/editconteudo/editconteudo.component';

const routes: Routes = [
  { path: '', component: LoginComponent },

  { path: 'Sites', component: SitesComponent },

  { path: 'Site', component: SiteComponent },
  { path: 'Utilizadores', component: UtilizadoresComponent },
  { path: 'Paginas', component: PaginasComponent },
  { path: 'Categorias', component: CategoriasComponent },
  { path: 'Artigos', component: ArtigosComponent },
  { path: 'Noticias', component: NoticiasComponent },
  { path: 'Eventos', component: EventosComponent },
  { path: 'Menus', component: MenusComponent },
  { path: 'Conteudos', component: ConteudosComponent },

  { path: 'Sites/Create', component: CreateSiteComponent },

  { path: 'Site/Edit', component: EditSiteComponent },

  { path: 'Paginas/Create', component: CreatePaginaComponent },
  { path: 'Paginas/Edit/:id', component: EditPaginaComponent },

  { path: 'Categorias/Create', component: CreateCategoriaComponent },
  { path: 'Categorias/Edit/:id', component: EditCategoriaComponent },

  { path: 'Artigos/Create', component: CreateArtigoComponent },
  { path: 'Artigos/Edit/:id', component: EditArtigoComponent },

  { path: 'Eventos/Create', component: CreateEventoComponent },

  { path: 'Menus/Edit/:id', component: EditMenuComponent },

  { path: 'Conteudos/Create', component: CreateConteudoComponent },
  { path: 'Conteudos/Edit/:id', component: EditConteudoComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }