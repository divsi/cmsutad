import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

import { LoginComponent } from './components/login/login.component';

import { SitesComponent } from './components/sites/sites.component';

import { SiteComponent } from './components/site/site.component';
import { UtilizadoresComponent } from './components/utilizadores/utilizadores.component';
import { PaginasComponent } from './components/paginas/paginas.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { ArtigosComponent } from './components/artigos/artigos.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { MenusComponent } from './components/menus/menus.component';
import { ConteudosComponent } from './components/conteudos/conteudos.component';

import { CreateSiteComponent } from './components/createsite/createsite.component';

import { EditSiteComponent } from './components/editsite/editsite.component';

import { CreatePaginaComponent } from './components/createpagina/createpagina.component';
import { EditPaginaComponent } from './components/editpagina/editpagina.component';

import { CreateCategoriaComponent } from './components/createcategoria/createcategoria.component';
import { EditCategoriaComponent } from './components/editcategoria/editcategoria.component';

import { CreateArtigoComponent } from './components/createartigo/createartigo.component';
import { EditArtigoComponent } from './components/editartigo/editartigo.component';

import { CreateEventoComponent } from './components/createevento/createevento.component';

import { EditMenuComponent } from './components/editmenu/editmenu.component';

import { CreateConteudoComponent } from './components/createconteudo/createconteudo.component';
import { EditConteudoComponent } from './components/editconteudo/editconteudo.component';

import { LoginService } from './services/login.service';
import { ApiService } from './services/api.service';
import { ToolbarService } from './services/toolbar.service';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,

    LoginComponent,

    SitesComponent,
    SiteComponent,
    UtilizadoresComponent,
    PaginasComponent,
    CategoriasComponent,
    ArtigosComponent,
    NoticiasComponent,
    EventosComponent,
    MenusComponent,
    ConteudosComponent,

    CreateSiteComponent,
    
    EditSiteComponent,

    CreatePaginaComponent,
    EditPaginaComponent,

    CreateCategoriaComponent,
    EditCategoriaComponent,

    CreateArtigoComponent,
    EditArtigoComponent,

    CreateEventoComponent,

    EditMenuComponent,

    CreateConteudoComponent,
    EditConteudoComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,

    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,

    AppRoutingModule
  ],
  providers: [LoginService, ApiService, ToolbarService],
  bootstrap: [AppComponent]
})
export class AppModule { }