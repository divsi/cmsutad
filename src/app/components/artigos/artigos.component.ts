import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { ArtigosDataSource } from './artigos-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-artigos',
  templateUrl: './artigos.component.html',
  styleUrls: ['./artigos.component.css']
})
export class ArtigosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ArtigosDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['designacao', 'publicado', 'editartigo', 'deleteartigo'];

  wasDeleteSuccessful: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("NOVO ARTIGO", "/Artigos/Create");

    this.wasDeleteSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new ArtigosDataSource(this.conn, this.paginator, this.sort);

    this.dataSource.getSiteArtigos();
  }

  filter(query: string) {
    
  }

  delete(idItem: number) {
    this.conn.deleteItemChildRelItem2Item(idItem).subscribe(
      (res: any) => {
        if(res >= 0)
        {
          this.conn.deleteItem(idItem).subscribe(
            (res: any) => {
              if(res == 1)
              {
                this.wasDeleteSuccessful = true;
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Artigo Eliminado.", "OK", {
        duration: 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Artigo!", "OK", {
        duration: 2000
      });
    }
  }
}