import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { CategoriasDataSource } from './categorias-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CategoriasDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['designacao', 'editcategoria', 'deletecategoria'];

  wasDeleteSuccessful: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList('NOVA CATEGORIA', '/Categorias/Create');

    this.wasDeleteSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new CategoriasDataSource(this.paginator, this.sort, this.conn);
    this.dataSource.getSiteCategorias();
  }

  delete(idItem: number) {
    this.conn.deleteItemParentRelItem2Item(idItem).subscribe(
      (res: any) => {
        if(res >= 0)
        {
          this.conn.deleteItem(idItem).subscribe(
            (res: number) => {
              if(res == 1)
              {
                this.wasDeleteSuccessful = true;
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Categoria Eliminada.", "OK", {
        duration: 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Categoria!", "OK", {
        duration: 2000
      });
    }
  }
}
