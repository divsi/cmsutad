import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { ConteudosDataSource } from './conteudos-datasource';
import { Observable, observable, forkJoin } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-conteudos',
  templateUrl: './conteudos.component.html',
  styleUrls: ['./conteudos.component.css']
})
export class ConteudosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ConteudosDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['designacao', 'texto', 'publicado', 'editconteudo', 'deleteconteudo'];

  wasDeleteSuccessful: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("NOVO CONTEÚDO", "/Conteudos/Create");

    this.wasDeleteSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new ConteudosDataSource(this.conn, this.paginator, this.sort);

    this.dataSource.getConteudos();
  }

  delete(idItem: number, texto: string) {
    this.conn.deleteItemChildRelItem2Item(idItem).subscribe(
      (result: any) => {
        if(result >= 0)
        {
          this.conn.deleteItem(idItem).subscribe(
            (result: any) => {
              if(result == 1)
              {
                this.conn.getItemsByFilename(texto).subscribe(
                  (results: any[]) => {
                    if(results.length == 1)
                    {
                      this.conn.deleteFile(texto).subscribe(
                        (result: any) => {
                          if(result == true)
                          {
                            this.wasDeleteSuccessful = true;
                          }
                        },
                        (error: any) => {
                          console.log(error);
                        }
                      );
                    }
                    else
                    {
                      this.wasDeleteSuccessful = true;
                    }
                  },
                  (error: any) => {
                    console.log(error);
                  }
                );
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Conteúdo Externo Eliminado.", "OK", {
        duration: 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Conteúdo Externo!", "OK", {
        duration: 2000
      });
    }
  }
}
