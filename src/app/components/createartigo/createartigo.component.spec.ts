import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateartigoComponent } from './createartigo.component';

describe('CreateartigoComponent', () => {
  let component: CreateartigoComponent;
  let fixture: ComponentFixture<CreateartigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateartigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateartigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
