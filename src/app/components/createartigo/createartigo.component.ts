import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Item {
  designacao: string;
  texto: string;
  publicado: number;
}

export interface Categoria {
  idItem: number;
  designacao: string;
  isSelected: boolean;
}

export interface RelItem2Item {
  idItemParent: number;
  idItemChild: number;
}

@Component({
  selector: 'app-createartigo',
  templateUrl: './createartigo.component.html',
  styleUrls: ['./createartigo.component.css']
})
export class CreateArtigoComponent implements OnInit {
  displayedColumns = ['select', 'designacao'];
  
  newArtigo: Item;
  publicar: boolean;

  categorias: Categoria[];
  newRelItem2Item: RelItem2Item;

  constructor(private conn: ApiService, private router: Router) { 
    this.newArtigo = { designacao: "", texto: "", publicado: 0 };
    this.publicar = false;

    this.categorias = [];
    this.newRelItem2Item = { idItemParent: 0, idItemChild: 0 };
  }

  ngOnInit() { 
    this.conn.getSiteCategorias(localStorage.getItem("idSite")).subscribe(
      (results: any[]) => {
        this.categorias = results;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  submit() {
    if(this.newArtigo.designacao && this.newArtigo.texto)
    {
      if(this.publicar == true)
      {
        this.newArtigo.publicado = 1;
      }

      this.conn.createSiteArtigo(this.newArtigo, localStorage.getItem("idSite")).subscribe(
        (res: any[]) => {
          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
          {
            let errorOnNewRelItem2Item = false;

            this.newRelItem2Item.idItemChild = res[0]["LAST_INSERT_ID()"];

            for(var i = 0; i < this.categorias.length; i++)
            {
              if(this.categorias[i].isSelected == true)
              {
                this.newRelItem2Item.idItemParent = this.categorias[i].idItem;

                this.conn.createRelItem2Item(this.newRelItem2Item).subscribe(
                  (res: any[]) => {
                    if(res[0]["LAST_INSERT_ID()"] == null || res[0]["LAST_INSERT_ID()"] == 0)
                    {
                      //Se ocorrer um erro em associar qualquer uma das categorias ao novo item, para todo o processo
                      errorOnNewRelItem2Item = true;
                    }
                  },
                  (error: any) => {
                    console.log(error);
                  }
                );
              }
            }

            //Se todo o processo de criação do novo artigo correu bem, redirecciona o utilizador de volta para a página de artigos
            if(errorOnNewRelItem2Item == false)
            {
              this.router.navigate(['/Artigos']);
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}