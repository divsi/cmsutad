import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Categoria {
  designacao: string;
}

@Component({
  selector: 'app-createcategoria',
  templateUrl: './createcategoria.component.html',
  styleUrls: ['./createcategoria.component.css']
})
export class CreateCategoriaComponent implements OnInit {
  newCategoria: Categoria;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();

    this.newCategoria = { designacao: "" };
  }

  ngOnInit() { }

  submit() {
    if(this.newCategoria.designacao)
    {
      this.conn.createSiteCategoria(this.newCategoria, localStorage.getItem("idSite")).subscribe(
        (res: any[]) => {
          console.log(res);
  
          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
          {
            this.router.navigate(['/Categorias']);
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}