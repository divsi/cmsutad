import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateconteudoComponent } from './createconteudo.component';

describe('CreateconteudoComponent', () => {
  let component: CreateconteudoComponent;
  let fixture: ComponentFixture<CreateconteudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateconteudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateconteudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
