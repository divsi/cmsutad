import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Item {
  designacao: string;
  texto: string;
  publicado: number;
}

export interface Categoria {
  idItem: number;
  designacao: string;
  isSelected: boolean;
}

export interface RelItem2Item {
  idItemParent: number;
  idItemChild: number;
}

@Component({
  selector: 'app-createconteudo',
  templateUrl: './createconteudo.component.html',
  styleUrls: ['./createconteudo.component.css']
})
export class CreateConteudoComponent implements OnInit {
  displayedColumns = ['select', 'designacao'];
  
  newConteudo: Item;
  file: File;  
  publicar: boolean;

  categorias: Categoria[];
  newRelItem2Item: RelItem2Item;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();
    
    this.newConteudo = { designacao: "", texto: "", publicado: 0 };
    this.file = null;
    this.publicar = false;

    this.categorias = [];
    this.newRelItem2Item = { idItemParent: 0, idItemChild: 0 };
  }

  ngOnInit() {
    this.conn.getSiteCategorias(localStorage.getItem("idSite")).subscribe(
      (results: any[]) => {
        this.categorias = results;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  onFileChange(files: FileList) {
    this.file = files.item(0);
  }

  submit() {
    //Verifica se o utilizador introduziu um título e se submeteu um ficheiro para o novo conteúdo externo
    if(this.newConteudo.designacao && this.file)
    {
      //Define o estado de publicação
      if(this.publicar = true)
      {
        this.newConteudo.publicado = 1;
      }

      //Envia o ficheiro escolhido pelo utilizador para o servidor
      this.conn.uploadFile(this.file).subscribe(
        (res: any) => { 
          if(res == true)
          {
            //Se o ficheiro foi enviado para o servidor com sucesso, define no novo conteúdo o caminho desse ficheiro
            this.newConteudo.texto = this.file.name;

            //De seguida cria um novo item para representar o novo conteúdo externo
            this.conn.createSiteConteudo(this.newConteudo, localStorage.getItem("idSite")).subscribe(
              (res: any[]) => {
                //Se o novo item foi criado com sucesso, associa o novo item às categorias seleccionadas pelo utilizador
                if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
                {
                  let errorOnNewRelItem2Item = false;

                  this.newRelItem2Item.idItemChild = res[0]["LAST_INSERT_ID()"];

                  for(var i = 0; i < this.categorias.length; i++)
                  {
                    if(errorOnNewRelItem2Item == false)
                    {
                      if(this.categorias[i].isSelected == true)
                      {
                        this.newRelItem2Item.idItemParent = this.categorias[i].idItem;

                        this.conn.createRelItem2Item(this.newRelItem2Item).subscribe(
                          (res: any[]) => {
                            if(res[0]["LAST_INSERT_ID()"] == null || res[0]["LAST_INSERT_ID()"] == 0)
                            {
                              //Se ocorrer um erro em associar qualquer uma das categorias ao novo item, para todo o processo
                              errorOnNewRelItem2Item = true;
                            }
                          },
                          (error: any) => {
                            console.log(error);
                          }
                        );
                      }
                    }
                  }

                  //Se todo o processo de criação do novo conteúdo externo correu bem, redirecciona o utilizador de volta para a página de conteúdos externos
                  if(errorOnNewRelItem2Item == false)
                  {
                    this.router.navigate(['/Conteudos']);
                  }
                }
              },
              (error: any) => {
                console.log(error);
              }
            );
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}