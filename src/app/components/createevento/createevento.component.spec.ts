import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateeventoComponent } from './createevento.component';

describe('CreateeventoComponent', () => {
  let component: CreateeventoComponent;
  let fixture: ComponentFixture<CreateeventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateeventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateeventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
