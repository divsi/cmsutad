import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Evento {
  designacao: string;
  texto: string;
  dataInicio: Date;
  dataFim: Date;
  publicado: number;
}

@Component({
  selector: 'app-createevento',
  templateUrl: './createevento.component.html',
  styleUrls: ['./createevento.component.css']
})
export class CreateEventoComponent implements OnInit {
  newEvento: Evento;
  publicar: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();

    this.newEvento = { designacao: "", texto: "", dataInicio: null, dataFim: null, publicado: 0 };
    this.publicar = false;
  }

  ngOnInit() { }

  submit() {
    if(this.newEvento.designacao && this.newEvento.texto && this.newEvento.dataInicio && this.newEvento.dataFim)
    {
      if(this.publicar == true)
      {
        this.newEvento.publicado = 1;
      }

      this.conn.createSiteEvento(this.newEvento, localStorage.getItem('idSite')).subscribe(
        (res: any[]) => {
          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
          {
            this.router.navigate(['/Eventos']);
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}