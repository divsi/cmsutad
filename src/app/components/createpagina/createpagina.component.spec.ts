import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatepaginaComponent } from './createpagina.component';

describe('CreatepaginaComponent', () => {
  let component: CreatepaginaComponent;
  let fixture: ComponentFixture<CreatepaginaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatepaginaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatepaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
