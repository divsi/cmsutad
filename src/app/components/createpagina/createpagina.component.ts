import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Pagina {
  designacao: string;
  descricao: string;
}

export interface Banner {
  designacao: string;
  texto: string;
}

@Component({
  selector: 'app-createpagina',
  templateUrl: './createpagina.component.html',
  styleUrls: ['./createpagina.component.css']
})
export class CreatePaginaComponent implements OnInit {
  newPagina: Pagina;
  newPaginaID: number;
  usarBanner: boolean;
  file: File;
  newBanner: Banner;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();

    this.newPagina = { designacao: "", descricao: "" };
    this.newPaginaID = 0;
    this.usarBanner = false;
    this.file = null;
    this.newBanner = { designacao: "", texto: "" };
  }

  ngOnInit() { }

  onFileChange(files: FileList) {
    this.file = files.item(0);
  }

  submit() {
    if(this.newPagina.designacao && this.newPagina.descricao)
    {
      this.conn.createSitePagina(this.newPagina, localStorage.getItem("idSite")).subscribe(
        (res: any[]) => {
          console.log(res);

          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] >= 0)
          {
            if(this.usarBanner == true)
            {
              if(this.newBanner.designacao && this.file != null)
              {
                this.newPaginaID = res[0]["LAST_INSERT_ID()"];

                this.conn.uploadFile(this.file).subscribe(
                  (res: any) => {
                    console.log(res);

                    if(res == true)
                    {
                      this.newBanner.texto = this.file.name;

                      this.conn.createSitePaginaBanner(this.newBanner, this.newPaginaID, localStorage.getItem("idSite")).subscribe(
                        (res: any[]) => {
                          console.log(res);

                          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] >= 0)
                          {
                            this.router.navigate(['/Paginas']);
                          }
                        },
                        (error: any) => {
                          console.log(error);
                        }
                      );
                    }
                  },
                  (error: any) => {
                    console.log(error);
                  }
                );
              }
            }
            else
            {
              this.router.navigate(['/Paginas']);
            }
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}