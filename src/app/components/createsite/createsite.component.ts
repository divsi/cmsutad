import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';
import { resetFakeAsyncZone } from '@angular/core/testing';

export interface Site {
  designacao: string;
  descricao: string;
  logo: string;
  titulo: string;
}

export interface RelSiteUtilizador {
  IDSIte: number;
  idUtilizador: string;
}

@Component({
  selector: 'app-createsite',
  templateUrl: './createsite.component.html',
  styleUrls: ['./createsite.component.css']
})
export class CreateSiteComponent implements OnInit {
  newSite: Site;
  newRelSiteUtilizador: RelSiteUtilizador;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();

    this.newSite = { designacao: "", descricao: "", logo: "", titulo: "" };
    this.newRelSiteUtilizador = { IDSIte: 0, idUtilizador: localStorage.getItem('idUtilizador') };
  }

  ngOnInit() { }

  submit() {
    if(this.newSite.designacao && this.newSite.descricao && this.newSite.titulo)
    {
      this.conn.createSite(this.newSite).subscribe(
        (res: any[]) => {
          if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
          {
            this.newRelSiteUtilizador.IDSIte = res[0]["LAST_INSERT_ID()"];

            this.conn.createRelSiteUtilizador(this.newRelSiteUtilizador).subscribe(
              (res: any[]) => {
                if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
                {
                  this.router.navigate(['/Sites']);
                }
              },
              (error: any) => {
                console.log(error);
              }
            );
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}