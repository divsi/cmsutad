import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditartigoComponent } from './editartigo.component';

describe('EditartigoComponent', () => {
  let component: EditartigoComponent;
  let fixture: ComponentFixture<EditartigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditartigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditartigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
