import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Item {
  designacao: string;
  texto: string;
  publicado: number;
}

export interface Categoria {
  idItem: number;
  designacao: string;
  isSelected: boolean;
}

export interface RelItem2Item {
  idItemParent: number;
  idItemChild: number;
}

@Component({
  selector: 'app-editartigo',
  templateUrl: './editartigo.component.html',
  styleUrls: ['./editartigo.component.css']
})
export class EditArtigoComponent implements OnInit {
  displayedColumns = ['select', 'designacao'];
  
  idItem: number;
  editArtigo: Item;
  publicar: boolean;

  categorias: Categoria[];
  itemCategorias: RelItem2Item[];

  constructor(private router: Router, private route: ActivatedRoute, private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();
    
    this.idItem = 0;
    this.editArtigo = { designacao: "", texto: "", publicado: 0 };
    this.publicar = false;

    this.categorias = [];
    this.itemCategorias = [];
  }

  ngOnInit() {
    this.idItem = parseInt(this.route.snapshot.paramMap.get("id"));

    this.conn.getItem(this.idItem).subscribe(
      (item: any) => {
        this.editArtigo = item[0];

        if(this.editArtigo.publicado == 1)
        {
          this.publicar = true;
        }

        this.conn.getSiteCategorias(localStorage.getItem("idSite")).subscribe(
          (res: any[]) => {
            this.categorias = res;

            this.conn.getItemCategorias(this.idItem).subscribe(
              (res: any[]) => {
                this.itemCategorias = res;

                for(var i = 0; i < this.categorias.length; i++)
                {
                  this.categorias[i].isSelected = this.canSelectCategoria(this.categorias[i].idItem, this.itemCategorias);
                }
              },
              (error: any) => {
                console.log(error);
              }
            );
          },
          (error: any) => {
            console.log(error);
          }
        );
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  canSelectCategoria(idCategoria: number, itemCategorias: RelItem2Item[]) {
    for(var i = 0; i < itemCategorias.length; i++)
    {
      if(itemCategorias[i].idItemParent == idCategoria)
      {
        return true;
      }
    }
  
    return false;
  }

  submit() {
    //Começa por definir o novo estado de publicação do artigo
    if(this.publicar == true)
    {
      this.editArtigo.publicado = 1;
    }
    else
    {
      this.editArtigo.publicado = 0;
    }

    //De seguida atualiza as informações do item que representa o artigo
    this.conn.updateItem(this.editArtigo, this.idItem).subscribe(
      (res: any) => {
        if(res == 1)
        {
          
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    //Por fim atualiza as categorias associadas a este item
    this.conn.deleteItemChildRelItem2Item(this.idItem).subscribe(
      (res: any) => {
        let selectedCategorias = [];

        for(var i = 0; i < this.categorias.length; i++)
        {
          if(this.categorias[i].isSelected == true)
          {
            selectedCategorias.push(this.categorias[i]);
          }
        }

        for(var i = 0; i < selectedCategorias.length; i++)
        {
          this.conn.createRelItem2Item({ idItemParent: selectedCategorias[i].idItem, idItemChild: this.idItem }).subscribe(
            (res: any[]) => {

            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    this.router.navigate(['/Artigos']);
  }
}