import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Categoria {
  designacao: string;
}

@Component({
  selector: 'app-editcategoria',
  templateUrl: './editcategoria.component.html',
  styleUrls: ['./editcategoria.component.css']
})
export class EditCategoriaComponent implements OnInit {
  idCategoria: number;
  editCategoria: Categoria;

  constructor(private route: ActivatedRoute, private router: Router, private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();

    this.idCategoria = parseInt(this.route.snapshot.paramMap.get("id"));
    this.editCategoria = { designacao: "" };
  }

  ngOnInit() {
    this.conn.getCategoria(this.idCategoria).subscribe(
      (response: any[]) => {
        if(response[0] != null)
        {
          this.editCategoria = response[0];
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  submit() {
    this.conn.updateCategoria(this.editCategoria, this.idCategoria).subscribe(
      (res: any) => {
        console.log(res);
        
        if(res == 1)
        {
          this.router.navigate(['/Categorias']);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}