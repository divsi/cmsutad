import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditconteudoComponent } from './editconteudo.component';

describe('EditconteudoComponent', () => {
  let component: EditconteudoComponent;
  let fixture: ComponentFixture<EditconteudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditconteudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditconteudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
