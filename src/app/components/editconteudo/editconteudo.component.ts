import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, observable, forkJoin } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Item {
  designacao: string;
  texto: string;
  publicado: number;
}

export interface Categoria {
  idItem: number;
  designacao: string;
  isSelected: boolean;
}

export interface RelItem2Item {
  idItemParent: number;
  idItemChild: number;
}

@Component({
  selector: 'app-editconteudo',
  templateUrl: './editconteudo.component.html',
  styleUrls: ['./editconteudo.component.css']
})
export class EditConteudoComponent implements OnInit {
  displayedColumns = ['select', 'designacao'];

  idItem: number;
  oldConteudo: Item;
  editConteudo: Item;
  newFile: File;
  publicar: boolean;

  categorias: Categoria[];
  itemCategorias: RelItem2Item[];
  oldSelectedCategorias: Categoria[];
  editSelectedCategorias: Categoria[];
  editRelItem2Item: RelItem2Item;

  editRequests: any[];

  constructor(private router: Router, private route: ActivatedRoute, private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();
    
    this.idItem = parseInt(this.route.snapshot.paramMap.get("id"));
    this.oldConteudo = { designacao: "", texto: "", publicado: 0 };
    this.editConteudo = { designacao: "", texto: "", publicado: 0 };
    this.newFile = null;
    this.publicar = false;

    this.categorias = [];
    this.itemCategorias = [];
    this.oldSelectedCategorias = [];
    this.editSelectedCategorias = [];
    this.editRelItem2Item = { idItemParent: 0, idItemChild: this.idItem };

    this.editRequests = [];
  }

  ngOnInit() {
    this.conn.getItem(this.idItem).subscribe(
      (item: any[]) => {
        //Obtém os dados do conteúdo externo e duplica-os para duas variáveis
        //Uma dessas variáveis vai guardar os dados antigos do conteúdo externo, enquanto que a outra vai guardar os dados alterados
        //Depois podemos comparar estas duas variáveis para verificar se houve de facto alterações aos dados do conteúdo externo (se os dados não forem alterados não é necessário fazer nada)
        this.oldConteudo = item[0];

        if(this.oldConteudo.publicado == 1)
        {
          this.publicar = true;
        }

        this.editConteudo.designacao = this.oldConteudo.designacao;
        this.editConteudo.texto = this.oldConteudo.texto;
        this.editConteudo.publicado = this.oldConteudo.publicado;

        //De seguida obtém todas as categorias associadas ao site em geral
        this.conn.getSiteCategorias(localStorage.getItem("idSite")).subscribe(
          (response: any[]) => {
            this.categorias = response;

            //Depois obtém todas as categorias associadas ao item obtido anteriormente
            this.conn.getItemCategorias(this.idItem).subscribe(
              (response: any[]) => {
                this.itemCategorias = response;

                //Relaciona os dois arrays de categorias obtidos anteriormente, marcando na tabela as categorias que estão associadas ao item
                for(var i = 0; i < this.categorias.length; i++)
                {
                  this.categorias[i].isSelected = this.isCategoriaSelected(this.categorias[i].idItem, this.itemCategorias);

                  //Se esta categoria estiver associada ao item, coloca-a num array
                  //Assim podemos depois verificar se as categorias deste item foram alteradas ou não (se não foram alteradas não é necessário fazer nada)
                  if(this.categorias[i].isSelected == true)
                  {
                    this.oldSelectedCategorias.push(this.categorias[i]);
                  }
                }
              },
              (error: any) => {
                console.log(error);
              }
            );
          },
          (error: any) => {
            console.log(error);
          }
        );
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  onFileChange(files: FileList) {
    this.newFile = files[0];
  }

  submit() {
    if(this.editConteudo.designacao)
    {
      let canDeleteOldFile = false;

      //Começa por verificar se o ficheiro deste conteúdo externo pode ser eliminado
      //Se esse ficheiro estiver a ser usado por mais do que um item, o ficheiro não pode ser eliminado do servidor
      this.conn.getItemsByFilename(this.editConteudo.texto).subscribe(
        (results: any[]) => {
          if(results.length == 1)
          {
            canDeleteOldFile = true;
          }
  
          //Se a variável do ficheiro não se encontrar vazia, significa que um novo ficheiro foi seleccionado para este conteúdo
          if(this.newFile != null)
          {
            if(canDeleteOldFile == true)
            {
              this.editRequests.push(this.conn.deleteFile(this.editConteudo.texto));
            }
  
            this.editRequests.push(this.conn.uploadFile(this.newFile));
  
            this.editConteudo.texto = this.newFile.name;
          }
  
          //Define o novo estado de publicação do conteúdo
          if(this.publicar = true)
          {
            this.editConteudo.publicado = 1;
          }
          else
          {
            this.editConteudo.publicado = 0;
          }
  
          //De seguida atualiza as informações do item que representa o conteúdo
          if(this.editConteudo.designacao != this.oldConteudo.designacao || this.editConteudo.texto != this.oldConteudo.texto || this.editConteudo.publicado != this.oldConteudo.publicado)
          {
            this.editRequests.push(this.conn.updateItem(this.editConteudo, this.idItem));
          }
  
          //Por fim atualiza as categorias associadas a este item, mas só se essas categorias tiverem sido alteradas pelo utilizador
          for(var i = 0; i < this.categorias.length; i++)
          {
            if(this.categorias[i].isSelected == true)
            {
              this.editSelectedCategorias.push(this.categorias[i]);
            }
          }
  
          if(this.wereCategoriasChanged(this.oldSelectedCategorias, this.editSelectedCategorias) == true)
          {
            if(this.oldSelectedCategorias.length > 0)
            {
              this.editRequests.push(this.conn.deleteItemChildRelItem2Item(this.idItem));
            }
  
            this.editRelItem2Item.idItemChild = this.idItem;
  
            for(var i = 0; i < this.editSelectedCategorias.length; i++)
            {
              this.editRelItem2Item.idItemParent = this.editSelectedCategorias[i].idItem;
  
              this.editRequests.push(this.conn.createRelItem2Item(this.editRelItem2Item));
            }
          }
  
          //Executa todos os pedidos HTTP presentes no array de pedidos (observables)
          if(this.editRequests.length > 0)
          {
            forkJoin(this.editRequests).subscribe(
              (results: any[]) => {
                console.log(results);
    
                if(this.wasEditSuccessful(results) == true)
                {
                  this.router.navigate(["/Conteudos"]);
                }
              },
              (errors: any[]) => {
                console.log(errors);
              }
            );
          }
          else
          {
            this.router.navigate(["/Conteudos"]);
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }

  isCategoriaSelected(idCategoria: number, itemCategorias: RelItem2Item[]) {
    for(var i = 0; i < itemCategorias.length; i++)
    {
      if(itemCategorias[i].idItemParent == idCategoria)
      {
        return true;
      }
    }
  
    return false;
  }

  wereCategoriasChanged(oldSelectedCategorias: Categoria[], editSelectedCategorias: Categoria[]) {
    if(editSelectedCategorias.length != oldSelectedCategorias.length)
    {
      return true;
    }
    else
    {
      for(var i = 0; i < editSelectedCategorias.length; i++)
      {
        if(editSelectedCategorias[i] != oldSelectedCategorias[i])
        {
          return true;
        }
      }
    }

    return false;
  }

  wasEditSuccessful(results: any[]) {
    for(var i = 0; i < results.length; i++)
    {
      if(results[i] == false)
      {
        return false;
      }
    }

    return true;
  }
}