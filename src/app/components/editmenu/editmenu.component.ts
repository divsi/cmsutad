import {Component, Injectable} from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {CollectionViewer, SelectionChange} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, merge} from 'rxjs';
import {map} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { ApiService } from '../../services/api.service';

export interface MenuItemNode {
  idMenusItem: number;
  idItem: number;
  designacao: string;
  idTipoItemDefault: number;
}

export interface MenuItem {
  idItem: number;
  idParentMenuItem: number;
}

/** Flat node with expandable and level information */
export class DynamicFlatNode {
  constructor(public menuItem: MenuItemNode, public level: number = 1, public expandable: boolean = false,
              public isLoading: boolean = false) {}
}

export class DynamicDatabase {
  rootLevelNodes: MenuItemNode[];

  constructor() { 
    this.rootLevelNodes = [];
  }

  initialData(data: MenuItemNode[]): DynamicFlatNode[] {
    this.rootLevelNodes = data;

    //Por cada item de raíz obtido, instancia um DynamicFlatNode para apresentar na árvore
    return this.rootLevelNodes.map(menuItem => new DynamicFlatNode(menuItem, 0, true));
  }

  isExpandable(menuItems: MenuItemNode[]): boolean {
    if(menuItems.length > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}

@Injectable()
export class DynamicDataSource {

  dataChange: BehaviorSubject<DynamicFlatNode[]> = new BehaviorSubject<DynamicFlatNode[]>([]);

  idMenu: number;

  get data(): DynamicFlatNode[] { return this.dataChange.value; }
  set data(value: DynamicFlatNode[]) {
    this.treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(private treeControl: FlatTreeControl<DynamicFlatNode>, private database: DynamicDatabase, private route: ActivatedRoute, private conn: ApiService) {
    this.idMenu = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
    this.treeControl.expansionModel.onChange!.subscribe(change => {
      if ((change as SelectionChange<DynamicFlatNode>).added ||
        (change as SelectionChange<DynamicFlatNode>).removed) {
        this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
  }

  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
    if (change.added) {
      change.added.forEach((node) => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed.reverse().forEach((node) => this.toggleNode(node, false));
    }
  }
  
  toggleNode(node: DynamicFlatNode, expand: boolean) {
    node.isLoading = true;

    this.conn.getMenuItems(this.idMenu, node.menuItem.idMenusItem).subscribe(
      (items: MenuItemNode[]) => {
        const children = items;
        const index = this.data.indexOf(node);
        if (!children || index < 0) {
          return;
        }
        
        if (expand) {
          const nodes = children.map(menuItem => new DynamicFlatNode(menuItem, node.level + 1, this.database.isExpandable(items)));
          
          this.data.splice(index + 1, 0, ...nodes);
        } 
        else 
        {
          this.data.splice(index + 1, children.length);
        }

        this.dataChange.next(this.data);

        node.isLoading = false;
      },
      (error: any) => {
        node.isLoading = false;

        console.log(error);
      }
    );
  }
}

@Component({
  selector: 'app-editmenu',
  templateUrl: 'editmenu.component.html',
  styleUrls: ['editmenu.component.css'],
  providers: [DynamicDatabase]
})
export class EditMenuComponent {
  idMenu: number;

  newMenuItem: MenuItem;

  constructor(database: DynamicDatabase, private route: ActivatedRoute, private conn: ApiService) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new DynamicDataSource(this.treeControl, database, route, conn);

    this.idMenu = parseInt(this.route.snapshot.paramMap.get('id'));
    this.newMenuItem = { idItem: 0, idParentMenuItem: 0 };

    //Com o ID do menu obtido anteriormente, obtém os items de raíz do menu
    //Depois passa esses itens obtidos para a DynamicDatabase acima
    this.conn.getMenuItems(this.idMenu, 0).subscribe(
      (items: any[]) => {   
        this.dataSource.data = database.initialData(items);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  treeControl: FlatTreeControl<DynamicFlatNode>;

  dataSource: DynamicDataSource;

  getLevel = (node: DynamicFlatNode) => { return node.level; };

  isExpandable = (node: DynamicFlatNode) => { return node.expandable; };

  hasChild = (_: number, _nodeData: DynamicFlatNode) => { return _nodeData.expandable; };

  addMenuItem(node: DynamicFlatNode) {
    this.treeControl.collapse(node);

    this.newMenuItem = { idItem: 1, idParentMenuItem: node.menuItem.idMenusItem };

    this.conn.createMenuItem(this.newMenuItem, this.idMenu).subscribe(
      (res: any) => {
        if(res[0]["LAST_INSERT_ID()"] != null && res[0]["LAST_INSERT_ID()"] != 0)
        {
          this.treeControl.expand(node);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  removeMenuItem(node: DynamicFlatNode) {
    this.conn.deleteMenuItem(node.menuItem.idMenusItem).subscribe(
      (res: any) => {
        console.log(res);

        if(res == 1)
        {
          
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}