import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpaginaComponent } from './editpagina.component';

describe('EditpaginaComponent', () => {
  let component: EditpaginaComponent;
  let fixture: ComponentFixture<EditpaginaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpaginaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditpaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
