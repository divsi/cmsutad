import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, observable, forkJoin } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Pagina {
  designacao: string;
  descricao: string;
}

export interface Banner {
  designacao: string;
  texto: string;
}

@Component({
  selector: 'app-editpagina',
  templateUrl: './editpagina.component.html',
  styleUrls: ['./editpagina.component.css']
})
export class EditPaginaComponent implements OnInit {
  idPagina: number;
  oldPagina: Pagina;
  editPagina: Pagina;

  idBanner: number;
  editBanner : Banner;
  oldPaginaHasBanner: boolean;

  usarBanner: boolean;
  file: File;
  canDeleteOldFile: boolean;

  editRequests: any[];

  constructor(private route: ActivatedRoute, private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();
    
    this.idPagina = parseInt(this.route.snapshot.paramMap.get("id"));
    this.oldPagina = { designacao: "", descricao: "" };
    this.editPagina = { designacao: "", descricao: "" };

    this.idBanner = 0;
    this.editBanner = { designacao: "", texto: "" };
    this.oldPaginaHasBanner = false;

    this.usarBanner = false;
    this.file = null;
    this.canDeleteOldFile = false;

    this.editRequests = [];
  }

  ngOnInit() {
    this.conn.getPagina(this.idPagina).subscribe(
      (pagina: any[]) => {
        if(pagina.length == 1)
        {
          //Obtém os dados da página e duplica-os para duas variáveis
          //Uma dessas variáveis vai guardar os dados antigos da página, enquanto que a outra vai guardar os dados alterados
          //Depois podemos comparar estas duas variáveis para verificar se houve de facto alterações aos dados da página (se os dados não forem alterados não é necessário fazer nada)
          this.oldPagina = pagina[0];

          this.editPagina.designacao = this.oldPagina.designacao;
          this.editPagina.descricao = this.oldPagina.descricao;

          //De seguida verifica se essa página possui um banner
          //Se possuir, obtém também os dados desse item
          this.conn.getPaginaBanner(this.idPagina).subscribe(
            (banner: any[]) => {
              if(banner.length == 1)
              {
                this.idBanner = banner[0].idItem;
                this.editBanner = banner[0];

                this.oldPaginaHasBanner = true;
                this.usarBanner = true;

                //Por fim verifica se o ficheiro do banner está a ser usado por mais items do site
                //Se o ficheiro do banner estiver a ser usado por mais items, não se pode apagar esse ficheiro do servidor
                this.conn.getItemsByFilename(this.editBanner.texto).subscribe(
                  (res: any[]) => {
                    if(res.length == 1)
                    {
                      this.canDeleteOldFile = true;
                    }
                  },
                  (error: any) => {
                    console.log(error);
                  }
                );
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  onFileChange(files: FileList) {
    this.file = files.item(0);
  }

  submit() {
    if(this.editPagina.designacao && this.editPagina.descricao)
    {
      if(this.editPagina.designacao != this.oldPagina.designacao || this.editPagina.descricao != this.oldPagina.descricao)
      {
        this.editRequests.push(this.conn.updatePagina(this.editPagina, this.idPagina));
      }

      //De seguida, só atualiza o banner da página se o utilizador tiver marcado a checkbox respetive e se tiver escolhido um ficheiro
      if(this.usarBanner == true)
      {
        if(this.editBanner.designacao && this.file != null)
        {
          //Se a página já tinha um banner antes da edição, apaga o ficheiro do banner antigo
          if(this.oldPaginaHasBanner == true)
          {
            if(this.canDeleteOldFile == true)
            {
              this.editRequests.push(this.conn.deleteFile(this.editBanner.texto));
            }
          }

          //Envia para o servidor o novo ficheiro para o banner da página
          //Depois atualiza as informações do banner com o nome do novo ficheiro enviado
          this.editRequests.push(this.conn.uploadFile(this.file));

          this.editBanner.texto = this.file.name;

          if(this.oldPaginaHasBanner == true)
          {
            this.editRequests.push(this.conn.updateBanner(this.editBanner, this.idBanner));
          }
          else
          {
            this.editRequests.push(this.conn.createSitePaginaBanner(this.editBanner, this.idPagina, localStorage.getItem("idSite")));
          }
        }
      }
      else
      {
        if(this.oldPaginaHasBanner == true)
        {
          if(this.canDeleteOldFile == true)
          {
            this.editRequests.push(this.conn.deleteFile(this.editBanner.texto));
          }

          this.editRequests.push(this.conn.deleteItem(this.idBanner));
        }
      }

      //Executa todos os pedidos HTTP presentes no array de pedidos (observables)
      forkJoin(this.editRequests).subscribe(
        (results: any[]) => {
          console.log(results);

          if(this.wasEditSuccessful(results) == true)
          {
            this.router.navigate(['/Paginas']);
          }
        },
        (errors: any[]) => {
          console.log(errors);
        }
      );
    }
  }

  wasEditSuccessful(results: any[]) {
    for(var i = 0; i < results.length; i++)
    {
      if(results[i] == false)
      {
        return false;
      }
    }

    return true;
  }
}