import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Site {
  designacao: string;
  descricao: string;
  logo: string;
  titulo: string;
}

@Component({
  selector: 'app-editsite',
  templateUrl: './editsite.component.html',
  styleUrls: ['./editsite.component.css']
})
export class EditSiteComponent implements OnInit {
  editSite: Site;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private router: Router) { 
    this.toolbar.clearButtonList();

    this.editSite = { designacao: "", descricao: "", logo: "", titulo: "" };
  }

  ngOnInit() { 
    this.conn.getSite(localStorage.getItem('idSite')).subscribe(
      (results: any[]) => {
        if(results.length == 1)
        {
          this.editSite = results[0];
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  submit() {
    if(this.editSite.designacao && this.editSite.descricao && this.editSite.titulo)
    {
      this.conn.updateSite(this.editSite, localStorage.getItem('idSite')).subscribe(
        (res: any) => {
          if(res == 1)
          {
            this.router.navigate(['/Site']);
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}