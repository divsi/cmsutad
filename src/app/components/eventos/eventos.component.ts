import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { EventosDataSource } from './eventos-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: EventosDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['designacao', 'dataInicio', 'dataFim', 'publicado', 'editevento', 'deleteevento'];

  wasDeleteSuccessful: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList('NOVO EVENTO', '/Eventos/Create');
  }

  ngOnInit() {
    this.dataSource = new EventosDataSource(this.paginator, this.sort, this.conn);

    this.dataSource.getSiteEventos();
  }

  delete(idItem: number) {
    this.conn.deleteItemChildRelItem2Item(idItem).subscribe(
      (res: any) => {
        if(res >= 0)
        {
          this.conn.deleteItem(idItem).subscribe(
            (res: any) => {
              if(res == 1)
              {
                this.wasDeleteSuccessful = true;
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Evento Eliminado.", "OK", {
        duration: 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Evento!", "OK", {
        duration: 2000
      });
    }
  }
}
