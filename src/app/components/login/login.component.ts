import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { LoginService } from '../../services/login.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Utilizador {
  idUtilizadores: string;
  username: string;
  nome: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  utilizador: Utilizador;

  constructor(private router: Router, private conn: ApiService, private loginService: LoginService, private toolbar: ToolbarService) {   
    this.toolbar.setTitle("cmsUTAD");
    
    if(this.loginService.isLoggedIn() == true)
    {
      this.router.navigate(['/Sites']);
    }
    else
    {
      this.utilizador = { idUtilizadores: "", username: "", nome: "" };
    }
  }

  ngOnInit() { }

  submit() {
    if(this.utilizador.username)
    {
      this.conn.getUtilizadorByUsername(this.utilizador.username).subscribe(
        (results: any[]) => {
          if(results.length == 1)
          {
            this.utilizador = results[0];
  
            this.loginService.login(this.utilizador.idUtilizadores);
  
            this.router.navigate(['/Sites']);
          }
          else
          {
            console.log("Credenciais inválidas!");
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}