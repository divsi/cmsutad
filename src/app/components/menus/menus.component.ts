import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MenusDataSource } from './menus-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MenusDataSource;

  wasRemoveSuccessful: boolean;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['descricao', 'editmenu', 'deactivatemenu'];

  constructor(private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("ADICIONAR MENU", "");

    this.wasRemoveSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new MenusDataSource(this.conn, this.paginator, this.sort);

    this.dataSource.getSiteMenus();
  }

  deactivate(idMenu: number) {

  }
}
