import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { NoticiasDataSource } from './noticias-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'components/noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: NoticiasDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['designacao', 'publicado', 'editnoticia', 'deletenoticia'];

  wasDeleteSuccessful: boolean;

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("NOVA NOTÍCIA", "Noticias/Create");

    this.wasDeleteSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new NoticiasDataSource(this.paginator, this.sort, this.conn);
    this.dataSource.getSiteNoticias();
  }

  delete(idItem: number) {
    this.conn.deleteItemChildRelItem2Item(idItem).subscribe(
      (res: any) => {
        if(res >= 0)
        {
          this.conn.deleteItem(idItem).subscribe(
            (res: any) => {
              if(res == 1)
              {
                this.wasDeleteSuccessful = true;
              }
            },
            (error: any) => {
              console.log(error);
            }
          );
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Notícia Eliminada.", "OK", {
        duration: 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Notícia!", "OK", {
        duration: 2000
      });
    }
  }
}
