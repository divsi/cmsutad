import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { PaginasDataSource } from './paginas-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-paginas',
  templateUrl: './paginas.component.html',
  styleUrls: ['./paginas.component.css']
})
export class PaginasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: PaginasDataSource;

  wasDeleteSuccessful: boolean;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['descricao', 'editpagina', 'deletepagina'];

  constructor(private conn: ApiService, private toolbar: ToolbarService, private snackbar: MatSnackBar) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("NOVA PÁGINA", "/Paginas/Create");

    this.wasDeleteSuccessful = false;
  }

  ngOnInit() {
    this.dataSource = new PaginasDataSource(this.conn, this.paginator, this.sort);
    this.dataSource.getSitePaginas();
  }

  delete(idPagina: number) {
    this.conn.deletePagina(idPagina).subscribe(
      (res: any) => {
        if(res == 1)
        {
          this.wasDeleteSuccessful = true;
        }
      },
      (error: any) => {
        console.log(error);
      }
    );

    if(this.wasDeleteSuccessful == true)
    {
      this.snackbar.open("Página Eliminada.", "OK", {
        duration : 2000
      });
    }
    else
    {
      this.snackbar.open("Erro ao Eliminar Página!", "OK", {
        duration : 2000
      });
    }
  }
}