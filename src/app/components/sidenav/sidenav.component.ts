import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { LoginService } from '../../services/login.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, private loginService: LoginService, private toolbar: ToolbarService) { }

  ngOnInit() {
    
  }

  logout() {
    this.toolbar.clearButtonList();

    this.loginService.logout();

    this.router.navigate(['']);
  }
}