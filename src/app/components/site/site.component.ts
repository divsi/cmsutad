import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service';

export interface Site {
  idSites: number;
  descricao: string;
  logo: string;
  titulo: string;
}

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})
export class SiteComponent implements OnInit {   
  site: Site;
  
  constructor(private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList('EDITAR SITE', '/Site/Edit');
    
    this.site = { idSites: 0, descricao: "", logo: "", titulo: "" };
  }

  ngOnInit() {
    this.conn.getSite(localStorage.getItem("idSite")).subscribe(
      (results: any[]) => {
        if(results.length == 1)
        {
          this.site = results[0];
          this.toolbar.setTitle(this.site.titulo);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}
