import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { SitesDataSource } from './sites-datasource';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { LoginService } from '../../services/login.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: SitesDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['titulo', 'deletesite'];

  constructor(private router: Router, private conn: ApiService, private loginService: LoginService, private toolbar: ToolbarService) { 
    this.loginService.clearLoadedSite();

    this.toolbar.setTitle("Lista de sites");
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("NOVO SITE", "/Sites/Create");
  }

  ngOnInit() {
    this.dataSource = new SitesDataSource(this.conn, this.paginator, this.sort);

    this.dataSource.getSites();
  }

  loadSite(idSite: string) {
    this.loginService.loadSite(idSite);

    this.router.navigate(['/Site']);
  }

  delete(IDSIte: number) {
    
  }
}
