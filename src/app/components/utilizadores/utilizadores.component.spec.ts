
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilizadoresComponent } from './utilizadores.component';

describe('UtilizadoresComponent', () => {
  let component: UtilizadoresComponent;
  let fixture: ComponentFixture<UtilizadoresComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilizadoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UtilizadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
