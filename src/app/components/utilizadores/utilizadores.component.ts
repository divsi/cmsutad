import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { UtilizadoresDataSource } from './utilizadores-datasource';

import { ApiService } from '../../services/api.service';
import { ToolbarService } from '../../services/toolbar.service'; 

@Component({
  selector: 'app-utilizadores',
  templateUrl: './utilizadores.component.html',
  styleUrls: ['./utilizadores.component.css']
})
export class UtilizadoresComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: UtilizadoresDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['username', 'nome', 'removeutilizador'];

  constructor(private conn: ApiService, private toolbar: ToolbarService) { 
    this.toolbar.clearButtonList();
    this.toolbar.addButtonToList("ADICIONAR UTILIZADOR", "");
  }

  ngOnInit() {
    this.dataSource = new UtilizadoresDataSource(this.conn, this.paginator, this.sort);
    this.dataSource.getUtilizadores();
  }

  remove(idUtilizador: number) {

  }
}
