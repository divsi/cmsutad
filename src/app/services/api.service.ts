import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Site {
  designacao: string;
  descricao: string;
  logo: string;
  titulo: string;
}

export interface RelSiteUtilizador {
  IDSIte: number;
  idUtilizador: string;
}

export interface Item {
  designacao: string;
  texto: string;
  publicado: number;
}

export interface Pagina {
  designacao: string;
  descricao: string;
}

export interface Banner {
  designacao: string;
  texto: string;
}

export interface Categoria {
  designacao: string;
}

export interface RelItem2Item {
  idItemParent: number;
  idItemChild: number;
}

export interface Evento {
  designacao: string;
  texto: string;
  dataInicio: Date;
  dataFim: Date;
  publicado: number;
}

export interface MenuItem {
  idItem: number;
  idParentMenuItem: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(public httpClient: HttpClient) { }

  //Métodos de leitura de dados
  getUtilizadorByUsername(username: string) {
    return this.httpClient.get("http://localhost2/api.php/getutilizadorbyusername/" + username);
  }

  getUtilizadorByID(idUtilizadores: string) {
    return this.httpClient.get("http://localhost2/api.php/getutilizadorbyid/" + idUtilizadores);
  }

  getUtilizadorSites(idUtilizador: string) {
    return this.httpClient.get("http://localhost2/api.php/getutilizadorsites/" + idUtilizador);
  }

  getSite(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsite/" + idSite);
  }

  getSiteUtilizadores(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsiteutilizadores/" + idSite);
  }

  getSitePaginas(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsitepaginas/" + idSite);
  }

  getItemsByFilename(filename: string) {
    return this.httpClient.get("http://localhost2/api.php/getitemsbyfilename/" + filename);
  }

  getSiteCategorias(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsitecategorias/" + idSite);
  }

  getSiteDestaqueHomePageItems(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsitedestaquehomepageitems/" + idSite);
  }

  getSiteArtigos(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsiteartigos/" + idSite);
  }

  getSiteNoticias(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsitenoticias/" + idSite);
  }

  getSiteEventos(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsiteeventos/" + idSite);
  }

  getSiteMenus(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsitemenus/" + idSite);
  }

  getSiteConteudos(idSite: string) {
    return this.httpClient.get("http://localhost2/api.php/getsiteconteudos/" + idSite);
  }

  getItem(idItem: number) {
    return this.httpClient.get("http://localhost2/api.php/getitem/" + idItem);
  }

  getItemCategorias(idItem: number) {
    return this.httpClient.get("http://localhost2/api.php/getitemcategorias/" + idItem);
  }

  getPagina(idPaginas: number) {
    return this.httpClient.get("http://localhost2/api.php/getpagina/" + idPaginas);
  }

  getPaginaBanner(idPagina: number) {
    return this.httpClient.get("http://localhost2/api.php/getpaginabanner/" + idPagina);
  }

  getCategoria(idCategoria: number) {
    return this.httpClient.get("http://localhost2/api.php/getcategoria/" + idCategoria);
  }

  getCategoriaItems(idCategoria: number) {
    return this.httpClient.get("http://localhost2/api.php/getcategoriaitems/" + idCategoria);
  }

  getMenuItems(idMenu: number, idParent: number) {
    return this.httpClient.get("http://localhost2/api.php/getmenuitems/" + idMenu + "/" + idParent);
  }

  //Métodos de edição de dados
  updateItem(editItem: Item, idItem: number) {
    return this.httpClient.post("http://localhost2/api.php/updateitem/" + idItem, JSON.stringify(editItem));
  }

  deleteItem(idItem: number) {
    return this.httpClient.get("http://localhost2/api.php/deleteitem/" + idItem);
  }

  uploadFile(file: File) {
    const fileFormData: FormData = new FormData();

    fileFormData.append('file', file, file.name);

    return this.httpClient.post("http://localhost2/api.php/uploadfile", fileFormData);
  }

  deleteFile(fileName: string) {
    return this.httpClient.get("http://localhost2/api.php/deletefile/" + fileName);
  } 

  createRelItem2Item(relItem2Item: RelItem2Item) {
    return this.httpClient.post("http://localhost2/api.php/createrelitem2item", JSON.stringify(relItem2Item));
  }

  deleteItemParentRelItem2Item(idItemParent: number) {
    return this.httpClient.get("http://localhost2/api.php/deleteitemparentrelitem2item/" + idItemParent);
  }

  deleteItemChildRelItem2Item(idItemChild: number) {
    return this.httpClient.get("http://localhost2/api.php/deleteitemchildrelitem2item/" + idItemChild);
  }

  createSite(newSite: Site) {
    return this.httpClient.post("http://localhost2/api.php/createsite", JSON.stringify(newSite));
  }

  createRelSiteUtilizador(newRelSiteUtilizador: RelSiteUtilizador) {
    return this.httpClient.post("http://localhost2/api.php/createrelsiteutilizador", JSON.stringify(newRelSiteUtilizador));
  }

  updateSite(editSite: Site, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/updatesite/" + idSite, JSON.stringify(editSite));
  }

  createSitePagina(newPagina: Pagina, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsitepagina/" + idSite, JSON.stringify(newPagina));
  }

  createSitePaginaBanner(newBanner: Banner, idPagina: number, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsitepaginabanner/" + idSite + "/" + idPagina, JSON.stringify(newBanner));
  }

  updatePagina(editPagina: Pagina, idPagina: number) {
    return this.httpClient.post("http://localhost2/api.php/updatepagina/" + idPagina, JSON.stringify(editPagina));
  }

  updateBanner(editBanner: Banner, idBanner: number) {
    return this.httpClient.post("http://localhost2/api.php/updatebanner/" + idBanner, JSON.stringify(editBanner));
  }

  deletePagina(idPagina: number) {
    return this.httpClient.get("http://localhost2/api.php/deletepagina/" + idPagina);
  }

  createSiteCategoria(newCategoria: Categoria, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsitecategoria/" + idSite, JSON.stringify(newCategoria));
  }

  updateCategoria(editCategoria: Categoria, idCategoria: number) {
    return this.httpClient.post("http://localhost2/api.php/updatecategoria/" + idCategoria, JSON.stringify(editCategoria));
  }

  createSiteArtigo(newArtigo: Item, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsiteartigo/" + idSite, JSON.stringify(newArtigo));
  }

  createSiteEvento(newEvento: Evento, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsiteevento/" + idSite, JSON.stringify(newEvento));
  }

  createSiteConteudo(newConteudo: Item, idSite: string) {
    return this.httpClient.post("http://localhost2/api.php/createsiteconteudo/" + idSite, JSON.stringify(newConteudo));
  }

  createMenuItem(newMenuItem: MenuItem, idMenu: number) {
    return this.httpClient.post("http://localhost2/api.php/createmenuitem/" + idMenu, JSON.stringify(newMenuItem));
  }

  deleteMenuItem(idMenuItem: number) {
    return this.httpClient.get("http://localhost2/api.php/deletemenuitem/" + idMenuItem);
  }
}