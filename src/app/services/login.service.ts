import { Injectable, OnDestroy } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {

  constructor() { }

  ngOnDestroy() {
    localStorage.clear();
  }

  login(idUtilizador: string) {
    localStorage.setItem("idUtilizador", idUtilizador);
  }

  loadSite(idSite: string) {
    localStorage.setItem("idSite", idSite);
  }

  clearLoadedSite() {
    localStorage.removeItem("idSite");
  }

  isLoggedIn() {
    if(localStorage.getItem("idUtilizador") != null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  hasLoadedSite() {
    if(localStorage.getItem("idSite") != null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  logout() {
    localStorage.clear();
  }
}
