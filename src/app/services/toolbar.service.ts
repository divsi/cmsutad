import { Injectable } from '@angular/core';

export interface ToolbarButton {
  name: string;
  routerLink: string;
}

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {
  title: string;
  buttonList: ToolbarButton[];

  constructor() { 
    this.title = "";
    this.buttonList = [];
  }

  setTitle(title: string) {
    this.title = title;
  }

  clearTitle() {
    this.title = "";
  }

  addButtonToList(newName: string, newRouterLink: string) {
    this.buttonList.push({ name: newName, routerLink: newRouterLink });
  }

  removeButtonFromList(name: string) {
    this.buttonList.unshift(this.buttonList.find(b => b.name == name));
  }

  clearButtonList() {
    this.buttonList = [];
  }
}